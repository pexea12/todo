const sinon = require('sinon')
const chai = require('chai')
const expect = chai.expect

const mongoose = require('mongoose')
require('sinon-mongoose')

const Todo = require('../app/models/TodoModel')

describe('Get all todos', () => {
  it('should return all todos', (done) => {
    const TodoMock = sinon.mock(Todo)
    const expectedResult = { status: true, todo: [] }
    TodoMock.expects('find').yields(null, expectedResult)
    Todo.find((err, result) => {
      TodoMock.verify()
      TodoMock.restore()
      expect(result.status).to.be.true
      done()
    })
  })

  it('should return error', (done) => {
    const TodoMock = sinon.mock(Todo)
    const expectedResult = { status: false, error: 'Something went wrong' }
    TodoMock.expects('find').yields(expectedResult, null)
    Todo.find((err, result) => {
      TodoMock.verify()
      TodoMock.restore()
      expect(err.status).to.not.be.true
      done()
    })
  })
})

describe('Post a new todo', () => {
  it('should create new post', (done) => {
    const TodoMock = sinon.mock(new Todo({ todo: 'Save new todo from mock'}))
    const todo = TodoMock.object
    const expectedResult = { status: true }
    TodoMock.expects('save').yields(null, expectedResult)
    todo.save((err, result) => {
        TodoMock.verify()
        TodoMock.restore()
        expect(result.status).to.be.true
        done()
    })
  })

  it("should return error, if post not saved", (done) => {
      const TodoMock = sinon.mock(new Todo({ todo: 'Save new todo from mock'}))
      const todo = TodoMock.object
      const expectedResult = { status: false }
      TodoMock.expects('save').yields(expectedResult, null)
      todo.save((err, result) => {
          TodoMock.verify()
          TodoMock.restore()
          expect(err.status).to.not.be.true
          done()
      })
  })
})


describe("Update a new todo by id", () => {
  it("should updated a todo by id", (done) => {
    const TodoMock = sinon.mock(new Todo({ completed: true}))
    const todo = TodoMock.object
    const expectedResult = { status: true }
    TodoMock.expects('save').withArgs({ _id: 12345 }).yields(null, expectedResult)
    todo.save((err, result) => {
      TodoMock.verify()
      TodoMock.restore()
      expect(result.status).to.be.true
      done()
    })
  })
  
  it("should return error if update action is failed", (done) => {
    const TodoMock = sinon.mock(new Todo({ completed: true}))
    const todo = TodoMock.object
    const expectedResult = { status: false }
    TodoMock.expects('save').withArgs({_id: 12345}).yields(expectedResult, null)
    todo.save((err, result) => {
      TodoMock.verify()
      TodoMock.restore()
      expect(err.status).to.not.be.true
      done()
    })
  })
})

describe("Delete a todo by id", () => {
  it("should delete a todo by id", (done) => {
    const TodoMock = sinon.mock(Todo)
    const expectedResult = { status: true }
    TodoMock.expects('remove').withArgs({_id: 12345}).yields(null, expectedResult)
    Todo.remove({_id: 12345}, (err, result) => {
      TodoMock.verify()
      TodoMock.restore()
      expect(result.status).to.be.true
      done()
    })
  })
  
  it("should return error if delete action is failed", (done) => {
    const TodoMock = sinon.mock(Todo)
    const expectedResult = { status: false }
    TodoMock.expects('remove').withArgs({_id: 12345}).yields(expectedResult, null)
    Todo.remove({_id: 12345}, (err, result) => {
        TodoMock.verify()
        TodoMock.restore()
        expect(err.status).to.not.be.true
        done()
    })
  })
})