const Todo = require('../models/todo.model')

const TodoCtrl = {
  // Get all todos from the Database
  GetTodo(req, res) {
    Todo.find({}, (err, todos) => {
      if(err) {
        res.json({status: false, error: "Something went wrong"})
        return
      }
      res.json({ status: true, todo: todos })
    })
  },

  PostTodo(req, res) {
    const todo = new Todo(req.body)
    todo.save((err, todo) => {
      if(err) {
        res.json({ status: false, error: "Something went wrong" })
        return
      }
      res.json({ status: true, message: "Todo Saved!!" })
    })
  },

  UpdateTodo(req, res) {
    const completed = req.body.completed
    Todo.findById(req.params.id, (err, todo) => {
      todo.completed = completed
      todo.save((err, todo) => {
        if(err) 
          res.json({ status: false, error: "Status not updated" })
        
        res.json({status: true, message: "Status updated successfully"})
      })
    })
  },

  // Deleting a todo baed on an ID
  DeleteTodo(req, res) {
    Todo.remove({_id: req.params.id}, (err, todos) => {
      if(err) {
        res.json({ status: false, error: "Deleting todo is not successfull" })
        return
      }
      res.json({ status: true, message: "Todo deleted successfully!!" })
    })
  }
}

module.exports = TodoCtrl