const express = require('express')
const router = express.Router()

const Todo = require('../models/TodoModel')
const TodoController = require('../controllers/TodoController')(Todo)

router.get('/todo', TodoController.GetTodo)
router.post('/todo', TodoController.PostTodo)
router.delete('/todo/:id', TodoController.DeleteTodo)
router.put('/todo/:id', TodoController.UpdateTodo)

module.exports = router